fn main() {
    println!("Chapter 5.1");
	fn_struct();
	tuple_struct();
}

// example user struct
struct User {
	username: String,
	email: String,
	sign_in_count: u64,
	active: bool,
}

fn tuple_struct() {
	// Same like struct, but fields won't have names
	struct Color(i32, i32, i32);
	struct Point(i32, i32, i32);
	
	// easy instanciation of a tuple struct
	let black = Color(0, 0, 0);
	let origin = Point(0, 0, 0);
}

fn fn_struct() {
	// Using a mutable struct (rust does not allow having just some fields mutable):
	let mut user1 = User {
		email: String::from("someone@example.com"),
		username: String::from("someusername123"),
		active: true,
		sign_in_count: 1,
	};
	
	// accessing a field of a struct
	user1.email = String::from("anotherone@example.com");

	let user2 = build_user(String::from("someone@example.com"), String::from("someusername124"));

	// shorthand notation to add a new user
	let user3 = User {
		email: String::from("third@example.com"),
		username: String::from("anotherusername567"),
		..user1 // remaining fields have the same values as in user1
	};
}

fn build_user(email: String, username: String) -> User {
	// returns a new user struct to the calling statement
	User {
		email: email,
		username: username,
		active: true,
		sign_in_count: 1,
	}
}

fn build_user_shorthand(email: String, username: String) -> User {
	// shorthand syntax for using parameter in structs.
	User {
		email,
		username,
		active: true,
		sign_in_count: 1,
	}
}
