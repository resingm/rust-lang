mod int_proc {
	pub fn process() {
		let v = vec![5, 1, 7, 51, 8, 71, 581, 5, 3, 3, 2, 1, 47, 4, 8, 9, 3, 7, 6, 5, 4, 5, 364];
		
		let mean = mean(&v);
		let median = median(&v);
		let mode = mode(&v);
	
		println!("Mean: {}", mean);
		println!("Median: {}", median);
		println!("Mode: {}", mode);
	}

	fn mean(v: &[u32]) -> f64 {
		let mut sum = 0;
		
		for i in v {
			sum += i;
		}

		sum / v.len()
	}

	fn median(v: &[u32]) -> u32 {
		0
	}

	fn mode(v: &[u32]) -> u32 {
		0
	}
}

fn main() {
	int_proc::process();
}
