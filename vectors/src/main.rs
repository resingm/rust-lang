fn main() {
	// empty vector without any elements
	let v: Vec<i32> = Vec::new();
	
	// create vector out of macro
	let mut v = vec![1, 2, 3];
	
	// updating a vector
	v.push(4);
	v.push(5);
	
	// access a vector
	let third: &i32 = &v[2];
	println!("The third element is {}", third);

	match v.get(2) {
		Some(third) => println!("The third element is {}", third),
		None => println!("There is no third element."),
	}

	//let does_not_exist = &v[100]; // panics; Index 100 does not exist.
	let does_not_exist = v.get(100); // does not panic; Returns none, if not exists.

	// iterating over a vec
	for i in &v {
		println!("{}", i);	
	}	

	// iterate over mutable references
	for i in &mut vec![100, 32, 57] {
		*i += 50; // * is called the dereference operator to get the value of i before mutating it.
		println!("{}", i);
	}

	// Using an enum to store multiple types in a single vector
	enum SpreadsheetCell {
		Int(i32),
		Float(f64),
		Text(String),
	}

	let row = vec![
		SpreadsheetCell::Int(3),
		SpreadsheetCell::Text(String::from("blue")),
		SpreadsheetCell::Float(10.12),
	];

	// <-- v goes out of scope. The elements will be droped.
}
