/*
The "cargo new" command creates a new package. The Cargo.toml file gives us the package.
A package contains 0 or 1 library crates. It can also contain 0..* binary crates.
At least one crate (either lib or bin) needs to be contained.

Cargo.toml file gives a package.
src/main.rs is the main root of a binary crate and does not need to be stated in Cargo.toml
src/lib.rs indicates that the package contains a library crate with the same name as the package


Modules
Organizes code of a crate in groups for readability & easy reuse.
*/

fn main() {
    println!("Hello, world!");
}
