use crate::List::{Cons, Nil};
use std::rc::Rc;

/*
Reference counter (Rc) are counting the references to a variable. 
If Rc > 0, then the variable won't be dropped.
If Rc = 0, the variable will be dropped.
*/

enum List {
	Cons(i32, Rc<List>),
	Nil,
}


fn main() {
	let a = Rc::new(Cons(5, Rc::new(Cons(10, Rc::new(Nil)))));
	println!("count after creating a = {}", Rc::strong_count(&a));

	let b = Cons(3, Rc::clone(&a));
	println!("count after creating a = {}", Rc::strong_count(&a));
	
	{
		let c = Cons(4, Rc::clone(&a));
		println!("count after creating a = {}", Rc::strong_count(&a));
	}

	println!("count after c goes out of scope = {}", Rc::strong_count(&a));
}
