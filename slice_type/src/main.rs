fn main() {
	let mut s = String::from("hello world");

	let word = first_word(&s);
	let sword = second_word(&s);

	println!("{}", word);
	println!("{}", sword);
	s.clear();
}

fn first_word(s: &String) -> &str { // &str is a string slice type.
	let bytes = s.as_bytes();
	
	for (i, &item) in bytes.iter().enumerate() {
		if item == b' ' {
			return &s[0..i];
		}
	}

	&s[..] // returns the complete string; 0..1 = ..1; 1..len = 1..; 0..len = ..
}

fn second_word(s: &String) -> &str {
	let word = first_word(&s);
	let bytes = s.as_bytes();

	for (i, &item) in bytes.iter().enumerate() {
		if i > word.len() {
			if item == b' ' {
				return &s[word.len()..i];
			}
		}
	}

	&s[word.len()..]
}
