#[cfg(test)]
mod tests {
	use super::*;

    #[test]
    fn it_works() {
        assert_eq!(4, add_two(2));
    }
}

use add_one;

pub fn add_two(x: i32) -> i32 {
	add_one::add_one(
		add_one::add_one(x)
	)
}
