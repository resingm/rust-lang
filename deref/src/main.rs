use std::ops::Deref;

fn main() {
	let x = 5;
	let _y = &x;
	let _y = Box::new(x);
	let _y = MyBox::new(x);

	assert_eq!(5, x);
	assert_eq!(5, *_y);

	let m = MyBox::new(String::from("Rust"));
	hello(&m)
}

fn hello(name: &str) {
	println!("Hello, {}!", name);
}

struct MyBox<T>(T);

impl<T> MyBox<T> {
	fn new(x: T) -> MyBox<T> {
		MyBox(x)
	}
}

impl<T> Deref for MyBox<T> {
	type Target = T;

	fn deref(&self) -> &T {
		&self.0
	}
}
