/*
Traits are similar to interfaces in other languages. They define which traits a type has to offer:
*/
pub trait Summary {
	fn summarize_author(&self) -> String;

	// Expects a method summarize() that returns a string.
	fn summarize(&self) -> String {
		String::from("(Read more...)")	// default behaviour for a trait.
	}
}

pub struct NewsArticle {
	pub headline: String,
	pub location: String,
	pub author: String,
	pub content: String,
}

// This way one implements the trait "Summary" for the struct "NewsArticle"
impl Summary for NewsArticle {
	fn summarize_author(&self) -> String {
		format!("@{}", self.author)
	}

	fn summarize(&self) -> String {
		format!("{}, by {} ({})", self.headline, self.author, self.location)
	}
}

// pub fn notify<T: Summary>(item: T) {
pub fn notify(item: impl Summary) { // syntax sugar for above line of code
	println!("Breaking news! {}", item.summarize());
}


fn largest_i32(list: &[i32]) -> i32 {
	let mut largest = list[0];

	for &item in list.iter() {
		if item > largest {
			largest = item;
		}
	}

	largest
}

// to fix largest<T> we can use std::cmp::PartialOrd to compare generic types
// multiple traits on one type can be connected via "where" or "+"
fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
	let mut largest = list[0];
	
	for &item in list {
		if item > largest {
			largest = item;
		}
	}

	largest

}

struct Point<T, U> {
	x: T,
	y: U,
}

impl<T, U> Point<T, U> {
	fn x(&self) -> &T {
		&self.x
	}

	fn mixup<V, W>(self, other: Point<V, W>) -> Point<T, W> {
		Point {
			x: self.x,
			y: other.y,
		}
	}
}

fn main() {
	let point_i32 = Point { x: 5, y: 10 };
	let point_float = Point { x: 5.1, y: 10.1, };

	let p1 = Point { x: 5, y: 10.4 };
	let p2 = Point { x: "Hello", y: 'c'};
	let p3 = p1.mixup(p2);

	println!("p3.x = {}, p3.y = {}", p3.x, p3.y);
		
	let number_list = vec![32, 50, 25, 200, 65];

	let result = largest_i32(&number_list);

	println!("The largest number is {}", result);

	let result = largest(&number_list);
	println!("The largest generic is {}", result);	
	// traits:

	let news = NewsArticle {
		headline: String::from("I am learning rust"),
		location: String::from("in my humble room"),
		author: String::from("me of course"),
		content: String::from("Great progress so far."),
	};
	
	println!("1 new article: {}", news.summarize());	
}
