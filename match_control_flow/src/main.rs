#[derive(Debug)] // so we can inspect the state in a minute
enum UsState {
	Alabama,
	Alaska,
	// --snip--
}

enum Coin {
	Penny,
	Nickel,
	Dime,
	Quarter(UsState),
}

fn value_in_cents(coin: Coin) -> u8 {
	match coin {
		Coin::Penny => {
			println!("Lucky penny!");
			1
		},
		Coin::Nickel => 5,
		Coin::Dime => 10,
		Coin::Quarter(state) => {
			println!("State quarter from {:?}", state);
			25
		},
	}
}

/* Using the match operator for Option<T>::Some && None 
	matches are exhaustive: We have to define every single
		option of an enum. Unless, we use the _ placeholder
		to define a default behaviour.
*/
fn plus_one(x: Option<i32>) -> Option<i32> {
	match x {
		None => None,
		Some(i) => Some(i + 1),
	}
}

/*
if let:
if let is a less verbose way to handle values that match one pattern while ignoring the rest.
*/
fn print_three(some_u8_value: Option<u8>) {
	if let Some(3) = some_u8_value {
		println!("three");
	} else {
		// do something else
		// does the same like _ in matches
	}
}


fn main() {
	println!("A nickel has the value of {} cents.", value_in_cents(Coin::Nickel));
	value_in_cents(Coin::Penny);

	value_in_cents(Coin::Quarter(UsState::Alaska));

	let five = Some(5);
	let six = plus_one(five);
	let none = plus_one(None);


	// this is the exhaustive pattern where we have to use _ as a placeholder.
	let some_u8_value = 0u8;

	match some_u8_value {
		1 => println!("one"),
		3 => println!("three"),
		_ => (), // unit value; means nothing will happen.
	}
}
