fn main() {
	// match arms
	/*

	match expressions have a value and different arms to match:
	match VALUE {
		PATTERN => EXPRESSION,
		PATTERN => EXPRESSION,
		PATTERN => EXPRESSION,
	}
	*/

	// Conditional if let expression
	let favorite_color: Option<&str> = None;
	let is_tuesday = false;
	let age: Result<u8, _> = "34".parse();

	if let Some(color) = favorite_color {
		println!("Using your favorite color, {}, as the background", color);
	} else if is_tuesday {
		println!("Tuesday is green day!");
	} else if let Ok(age) = age {
		if age > 30 {
			println!("Using purple as the background color");
		} else {
			println!("Using orange as the background color.");
		}
	} else {
		println!("Using blue as the background color.");	
	}

	// while let Conditional loops
	let mut stack = Vec::new();

	stack.push(1);
	stack.push(2);
	stack.push(3);

	while let Some(top) = stack.pop() {
		println!("{}", top);
	}

	// for loops
	// for x in y ==> x is the pattern

	let v = vec!['a', 'b', 'c'];

	for (index, value) in v.iter().enumerate() {
		println!("{} is at index {}", value, index);
	}

	// let statements
	// let PATTERN = EXPRESSION
	// bind everything to the variable _x, whatever the value is.
	let _x = 5;
	let (_x, _y, _z) = (1, 2, 3);
	
	// function parameters
	// _x is the pattern
	fn _foo(_x: i32) {
		// code goes here.
	}
}
