#[derive(Debug)] // opt in that Rectangle can be printed in Debug
struct Rectangle {
	width: u32,
	height: u32,
}

impl Rectangle {
	/*
	Those are assiciated functions.
	They don't have &self as a parameter.
	Usually, constructors are making use of this.
	To call associated functions, the struct name :: is used.
	*/	
	fn square(size: u32) -> Rectangle {
		Rectangle { width: size, height: size }
	}
	
	/*
	Those are methods of an implemented block.
	&self is the first parameter of those statements.
	*/
	fn area(&self) -> u32 {
		self.width * self.height
	}

	fn can_hold(&self, other: &Rectangle) -> bool {
		self.width > other.width && self.height > other.height
	}
}

fn main() {
	/*
	This program will calculate the area of a rectangle.
	*/

	// rectangle 1
	let width1 = 30;
	let height1 = 50;
	
	// rectangle 2
	let rect2 = (30, 50);

	// rectangle 3
	let rect3 = Rectangle { width: 30, height: 50 };

	// rectangle 4
	let rect4 = Rectangle { width: 30, height: 50 };
	let rect5 = Rectangle { width: 10, height: 40 };
	let rect6 = Rectangle { width: 60, height: 45 };

	let sq = Rectangle::square(100);
	
	println!("Can rect4 hold rect5? {}", rect4.can_hold(&rect5));
	println!("Can rect4 hold rect6? {}", rect4.can_hold(&rect6));
		
	println!(
		"The area of rectangle 1 is {} square pixels.",
		area1(width1, height1)
	);
	println!(
		"The area of rectangle 2 is {} square pixels.",
		area2(rect2)
	);
	println!(
		"The area of rectangle 3 is {} square pixels.",
		area3(&rect3)
	);

	// This will print in debug:
	println!("rect3 is {:?}", rect3); // {:?} is a placeholder to print debug data of a struct
	println!("rect3 is {:#?}", rect3); // #? prints in multi-line format

	println!("The area() of rectangle 4 is {} square pixels.",
		rect4.area()
	);
}


fn area1(width: u32, height: u32) -> u32 {
	width * height
}

fn area2(dimensions: (u32, u32)) -> u32 {
	dimensions.0 * dimensions.1
}

fn area3(rect: &Rectangle) -> u32 {
	rect.width * rect.height
}
