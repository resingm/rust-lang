fn main() {
	let s = String::new();
	println!("Length of s: {}", s.len());	

	let mut s = "foo".to_string();
	s.push_str("bar");

	let s1 = String::from("Hello, ");
	let s2 = String::from("world!");
	let s3 = s1 + &s2; // s1 has been moved here & can no longer be used.
	
	println!("{}", s3);

	let s1 = String::from("tic");	
	let s2 = String::from("tac");	
	let s3 = String::from("toe");	
	
	let s = format!("{}-{}-{}", s1, s2, s3);

	println!("{}", s);
	
	/*
	Valid UTF-8 chars can be made out of multiple bytes. Accessing chars via indices isn't possible in rust.
	Alternatives are seen below:
	*/


	// iterating over strings
	// chars:
	for c in "hello".chars() {
		// do something with each char
	}
	// bytes
	for b in "hello".bytes() {
		// do somethings with each byte.
	}
}
