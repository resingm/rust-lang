fn main() {
	let s = String::from("hello"); // s comes into scope
	takes_ownership(s); // s's value moves into the function and so is no longer valid here
	
	let x = 5; // x comes into scope

	makes_copy(x); // x would move into the function, but i32 is Copy, so it's okay to still use x afterward
	
	let s1 = String::from("hello");	
	println!("The length of '{}' is {}.", s1, calculate_length_as_reference(&s1));

	let mut s2 = String::from("hello");
	mutable_ref(&mut s2);
	println!("{}", s2);
}

fn takes_ownership(some_string: String) { // some_string comes into scope
	println!("{}", some_string);
} // some_string goes out of scope; drop is called; backing memory is freed

fn makes_copy(some_integer: i32) { // some_integer comes into scope
	println!("{}", some_integer);
} // some_integer goes out of scope; nothing special happens

fn calculate_length_as_reference(s: &String) -> usize {
	s.len()
}

fn mutable_ref(s: &mut String) {
	s.push_str(", world!");
}


