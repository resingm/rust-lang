fn main() {
	let x = 1;
	match x {
		1 => println!("One"),
		2 => println!("Two"),
		_ => println!("Anything"),
	}

	let x = Some(5);
	let y = 10;

	match x {
		Some(50) => println!("Got 50"),
		Some(y) => println!("Matched, y = {:?}", y),
		_ => println!("Default case, x = {:?}", x),
	}

	println!("at the end: x = {:?}, y = {:?}", x, y);

	let y = 5;
	
	// matching multiple patterns
	match y {
		5 | 6 => println!("5 or 6"),
		_ => println!("anything"),
	}

	// matching ranges
	match y {
		1...5 => println!("one through five"),
		_ => println!("anything"),
	}

	// destructuring structs
	struct Point {
		x: i32,
		y: i32,
	}

	let p = Point { x: 0, y: 7 };

	// assign x to a and y to b
	let Point { x: a, y: b } = p;

	assert_eq!(0, a);
	assert_eq!(7, b);

	// shorthand notation (assigns x to a variable x and y to a variable y)
	let Point { x, y } = p; 

	assert_eq!(0, x);
	assert_eq!(7, y);

	// extra conditionals with match guards
	let num = Some(4);

	match num {
		Some(x) if x < 5 => println!("less than five: {}", x),
		Some(x) => println!("{}", x),
		None => (),
	}

	// @ bindings
	// create variables that holds values at the same time the value is tested.
	enum Message {
		Hello { id: i32 },
	}

	let msg = Message::Hello { id: 5 };

	match msg {
		Message::Hello { id: id_variable @ 3...7 } => {
			println!("Found an id in range: {}", id_variable);
		},
		Message::Hello { id } => {
			println!("Found some other id: {}", id);
		},
	}
}

