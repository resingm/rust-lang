use std::fs::File;
use std::io::ErrorKind;

fn main() {
	//panic!("crash and burn");	
	
	//let v = vec![1, 2, 3];
	//v[3];

	/*
	The result enum looks like this:
	enum Result<T, E> {
		Ok(T),
		Err(E),
	}
	*/
	
	// will fail, file does not exist
	let f = File::open("hello.txt");

	let _f = match f {
		Ok(file) => file,
		Err(error) => match error.kind() {
			ErrorKind::NotFound => match File::create("hello.txt") {
				Ok(fc) => fc,
				Err(e) => panic!("Problem creating the file: {:?}", e),
			},
			other_error => panic!("Problem opening the file: {:?}", other_error),
		},
	};
	
	// less verbose way to execute the above code.
	let _f = File::open("hello.txt").unwrap_or_else(|error| {
		if error.kind() == ErrorKind::NotFound {
			File::create("hello.txt").unwrap_or_else(|error| {
				panic!("Problem creating the file: {:?}", error);
			})
		} else {
			panic!("Problem opening the file: {:?}", error);
		}
	});

	// unwrap can automatically panic! Unwrap returns the value of Ok<T> or panics the error in Err<E>
	let _f = File::open("hello.txt").unwrap();	

	// expect is an alternative to unwrap. It will return the value of Ok<T> and throws the given error message:
	let _f = File::open("hello.txt").expect("Failed to open hello.txt");

	let _res = read_username_from_file();
	let _res = read_username_from_file_2();
	let _res = read_username_from_file_3();
}

/*
Errors can also be propagated to the calling function:
*/

use std::io;
use std::io::Read;

fn read_username_from_file() -> Result<String, io::Error> {
	let f = File::open("hello.txt");
	
	let mut f = match f {
		Ok(file) => file,
		Err(e) => return Err(e),
	};
	
	let mut s = String::new();

	match f.read_to_string(&mut s) {
		Ok(_) => Ok(s),
		Err(e) => Err(e),
	}
	
}

/*
The ? operator can shorten read_username_from_file()
It works as above: If the Result is an Ok, the value of Ok will be returned.
If the value is an Err, the Err will be returned.
*/

fn read_username_from_file_2() -> Result<String, io::Error> {
	let mut f = File::open("hello")?;
	let mut s = String::new();
	f.read_to_string(&mut s)?;
	Ok(s)
}

// Can also be written in one line:

fn read_username_from_file_3() -> Result<String, io::Error> {
	let mut s = String::new();
	File::open("hello.txt")?.read_to_string(&mut s)?;

	Ok(s)
}



