enum IpAddrKind {
	V4,
	V6,
}

// alternative to give enum variants already a value
enum AltIpAddr {
	V4(u8, u8, u8, u8),
	V6(String),
}

struct IpAddr {
	kind: IpAddrKind,
	address: String,
}

enum Message {
	Quit,
	Move { x: i32, y: i32 },
	Write(String),
	ChangeColor(i32, i32, i32),
}

impl Message {
	fn call(&self) {
		// method body would be defined here
	}
}

fn main() {
	let four = IpAddrKind::V4;
	let six = IpAddrKind::V6;

	let home = IpAddr {
		kind: IpAddrKind::V4,
		address: String::from("127.0.0.1"),
	};

	let loopback = IpAddr {
		kind: IpAddrKind::V6,
		address: String::from("::1"),
	};

	let alt_home = AltIpAddr::V4(127, 0, 0, 1);

	// message part comes here

	let m = Message::Write(String::from("hello world."));
	m.call();

	// Option enum (essential enum for rust)
	let some_number = Some(5); // same as Option<i32>::Some(5);
	let some_string = Some("a string");
	let absent_number: Option<i32> = None; // Option<T> with accepts a generic type
}
