fn main() {
    println!("Hello, world!");

	another_function(125, 126);
	expression_function();

	println!("Returning the value: {}", five());

	if_expression(false);
	if_expression(true);

	loops();
}

fn another_function(x: i32, y: i32) {
	println!("The value of x is: {}", x);
	println!("The value of y is: {}", y);
}

fn expression_function() {
	let y = 6; // <- this is a statement
	/*
	let x = (let y = 6); // <- this is an expression which has a statement on the right.
						 //    the statement does not return something, so it does not compile
	*/

	/*
	Expressions are everything that evaluates to a value. You can write an expression by keeping it in {} :

	{
		let x = 3;
		x + 1
	}

	This will create a var x and returns x + 1 (= 4).
	Adding a semicolon at the end of x + 1 will convert the expression to a statement, which then does not return anything.
	*/
}

fn five() -> i32 {
	5
}

fn if_expression(x : bool) {
	if x {
		println!("condition true");
	} else {
		println!("condition false");
	}	
}

fn loops() {
	println!("Print");
	let mut counter = 0;
	loop {
		println!("again");
		counter += 1;

		if counter == 10 {
			break; 
		}
	}
}
