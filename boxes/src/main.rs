fn main() {
	// Boxes are used to place data on the heap instead of the stack
	let b = Box::new(5);
	println!("b = {}", b);
}
